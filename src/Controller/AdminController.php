<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Categorie;
use App\Entity\Question;
use App\Entity\Reponse;
use App\Entity\Result;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends AbstractController
{

    public function index()
    {
        return $this->render("admin/adminIndex.html.twig");
    }

    public function users()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        dd($users);
        return $this->render("admin/adminUsers.html.twig");


    }

    public function quizz()
    {
        
    }
}
