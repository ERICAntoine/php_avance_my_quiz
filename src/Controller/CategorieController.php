<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Categorie;
use App\Entity\Question;
use App\Entity\Reponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;



class CategorieController extends AbstractController
{
    public function index()
    {
        $categorie = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        return $this->render("index.html.twig",  array("categorie" => $categorie));
    }

    public function questionByCategorie(Request $request, $idCategory, $idQuestion)
    {
        $question = $this->getDoctrine()->getRepository(Question::class)->findBy(["id_categorie" => $idCategory]);

        if($request->request->get("id_game") == NULL)
            $id_game = uniqid();

        if($idQuestion > count($question) - 1)
            return $this->redirectToRoute('result', ["id_game" => $request->request->get("id_game")]);
        
        $reponse = $this->getDoctrine()->getRepository(Reponse::class)->findBy(["id_question" => $question[$idQuestion]->id]);

        if($request->request->get("id_game") == NULL)
            return $this->render("question.html.twig",  array("questions" => $question[$idQuestion], "reponse" => $reponse, "idQuestion" => $idQuestion, 'id' => $idCategory, "id_game" => $id_game));
        else
            return $this->render("question.html.twig",  array("questions" => $question[$idQuestion], "reponse" => $reponse, "idQuestion" => $idQuestion, 'id' => $idCategory, "id_game" => $request->request->get("id_game")));

    }
}
