<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Categorie;
use App\Entity\Question;
use App\Entity\Reponse;
use App\Entity\Result;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;

class ReponseController extends AbstractController
{
    public function index()
    {
        $categorie = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        return $this->render("index.html.twig",  array("categorie" => $categorie));
    }

    public function questionByCategorie($id)
    {
        $question = $this->getDoctrine()->getRepository(Question::class)->findOneBy(["id_categorie" => $id]);
        $reponse = $this->getDoctrine()->getRepository(Reponse::class)->findAll(["id_question" => $question["id"]]);
        return $this->render("question.html.twig",  array("questions" => $question, "reponse" => $reponse));
    }

    public function reponse($idCat, $id, Request $request)
    {
        $user = $this->getUser();
        $userId = ($user != null) ? $user->id : null;
        $entityManager = $this->getDoctrine()->getManager();
        $result = new Result();
        $result->setIdResult($request->request->get("id_game"));
        $result->setIdReponse($request->request->get("idResponse"));
        if($userId != null)
        {
            $result->setIduser($userId);
        }
        $result->setIdCategorie($idCat);
        $result->setCreatedAt(date("D M j G:i:s T Y"));
        $result->setIdQuestion($id);
        $entityManager->persist($result);
        $entityManager->flush();
        $reponse = $this->getDoctrine()->getRepository(Reponse::class)->reponse($id);
        return $this->render("reponse.html.twig",  array("reponse" => $reponse, 'idCat' => $idCat, 'idQue' => $id, "idQestion" => json_decode($request->request->get("idQes")), "id_game" => $request->request->get("id_game")));
    }
}
