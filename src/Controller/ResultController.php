<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Categorie;
use App\Entity\Question;
use App\Entity\Reponse;
use App\Entity\Result;
use Symfony\Component\HttpFoundation\Request;

class ResultController extends AbstractController
{

    public function index($id_game)
    {
        $results = $this->getDoctrine()->getRepository(Result::class)->findBy(["id_result" => $id_game]);
        $question = $this->getDoctrine()->getRepository(Question::class)->findBy(["id_categorie" => $results[0]->id_categorie]);
        $reponse = $this->getDoctrine()->getRepository(Reponse::class)->findBy(["id_question" => $results[0]->id_question]);
        $rep = [];
        foreach($results as $key => $value)
        {
            $reponse = $this->getDoctrine()->getRepository(Reponse::class)->findBy(["id_question" => $results[$key]->id_question]);
            array_push($rep, $reponse);
        }

        return $this->render("result.html.twig", ["resultat" => $results, "questions" => $question, "reponse" => $rep, "countQ" => count($question)]);
    }

    public function history()
    {
        $results = $this->getDoctrine()->getRepository(Result::class)->findResult();
        return $this->render("history.html.twig", ["result" => $results]);
    }
}
