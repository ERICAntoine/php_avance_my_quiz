<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResultRepository")
 */
class Result
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */

    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */

    public $id_result;

    /**
     * @ORM\Column(type="integer", length=255)
     */

    public $id_reponse;

    /**
     *@ORM\Column(type="string", length=255)
     */

    public $createdAt;

    /**
     * @ORM\Column(type="integer", length=255)
     */

    public $id_user;

    /**
     * @ORM\Column(type="integer", length=255)
     */

    public $id_categorie;

    /**
     * @ORM\Column(type="integer", length=255)
     */

    public $id_question;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdResult(): ?string
    {
        return $this->id_result;
    }

    public function setIdResult(?string $id_result): self
    {
        $this->id_result = $id_result;

        return $this;
    }

    public function getIdReponse(): ?int
    {
        return $this->id_result;
    }

    public function setIdReponse(?int $id_reponse): self
    {
        $this->id_reponse = $id_reponse;

        return $this;
    }

    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?string $date): self
    {
        $this->createdAt = $date;

        return $this;
    }

    public function getIdUser(): ?int
    {
        return $this->id_result;
    }

    public function setIdUser(?int $id_user): self
    {
        $this->id_user = $id_user;

        return $this;
    }

    public function getIdCategorie(): ?int
    {
        return $this->id_result;
    }

    public function setIdCategorie(?int $id_categorie): self
    {
        $this->id_categorie = $id_categorie;

        return $this;
    }

    public function getIdQuestion(): ?int
    {
        return $this->id_result;
    }

    public function setIdQuestion(?int $id_question): self
    {
        $this->id_question = $id_question;

        return $this;
    }
}
